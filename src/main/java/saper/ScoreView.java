package saper;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class ScoreView extends JFrame implements Serializable {
	private static final long serialVersionUID = 4639884409924987861L;

	private JPanel panel = new JPanel();

	public ScoreView() {
		setContentPane(panel);
		panel.setPreferredSize(new Dimension(380, 380));
		setSize(400, 400);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
