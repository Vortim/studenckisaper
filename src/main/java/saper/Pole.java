package saper;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class Pole extends JButton implements Serializable {
    private static final long serialVersionUID = -7371332272806214365L;
    private boolean bomba;
    private int ileBomb = 0;
    private boolean showed = false;
    private boolean mark = false;
    private Color color = Color.WHITE;

    public void setIleBomb(int ileBomb) {
        this.ileBomb = ileBomb;
    }

    public int getIleBomb() {
        return ileBomb;
    }

    public void setBomba(boolean bomba) {
        this.bomba = bomba;
    }

    public boolean isBomba() {
        return bomba;
    }

    public void setShowed(boolean showed) {
        this.showed = showed;
    }

    public boolean isShowed() {
        return showed;
    }

    public void setMark(boolean mark) {
        this.mark = mark;
    }

    public boolean isMark() {
        return mark;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

}
