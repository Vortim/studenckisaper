package saper;

import java.io.Serializable;

public class RunnerSaper implements Serializable {
	private static final long serialVersionUID = -1490254275153384834L;
	protected static View view;

	public RunnerSaper() {
		view = new View();
	}

	public static void main(String[] args) {
		new RunnerSaper();
	}

}
