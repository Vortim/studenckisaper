package saper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.util.Random;

public class View extends JFrame implements Serializable {

	private static final long serialVersionUID = -2855122392384702606L;
	protected int ileBomb = 40, xPos = 30, yPos = 30;
	private JPanel panel = new JPanel();
	public Widok widok = new Widok();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu settingsM = new JMenu("Option");
	private JMenuItem settingsWindow = new JMenuItem("Settings");
	private JMenuItem scoresWindow = new JMenuItem("Scores");
	private JButton restart = new JButton("Restart");
	private JLabel time = new JLabel("0s");
	private JLabel bombsLeft = new JLabel(ileBomb + "");
	private long startTime = System.currentTimeMillis();
	private static Pole[][] plansza;
	protected boolean koniec = false;
	private boolean started = false;
	private View view;

	public View() {
		this.view = this;
		try {
			generuj();
		} catch (BombException e) {
			e.printStackTrace();
		}
		setTitle("Saper - ver. Beta");
		setContentPane(panel);
		setJMenuBar(menuBar);
		menuBar.setLayout(new GridLayout(1, 6));
		menuBar.add(restart);
		menuBar.add(settingsM);
		menuBar.add(new JLabel(""));
		menuBar.add(new JLabel(""));
		menuBar.add(bombsLeft);
		menuBar.add(time);
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					if (!started) {
						try {
							Thread.sleep(400);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					}
					if ((System.currentTimeMillis() - startTime) % 1000 == 0
							&& started && !koniec)
						time.setText((int) (System.currentTimeMillis() - startTime)
								/ 1000 + "s");
				}
			}
		}).start();
		settingsM.add(settingsWindow);
		settingsM.add(scoresWindow);
		scoresWindow.addActionListener(new AListener());
		settingsWindow.addActionListener(new AListener());
		restart.addActionListener(new AListener());
		panel.addMouseListener(new MListener());
		panel.setBackground(Color.LIGHT_GRAY);
		panel.add(widok);
		widok.setPreferredSize(new Dimension(xPos * 15 + 35, yPos * 15 + 85));
		setSize(new Dimension(xPos * 15 + 35, yPos * 15 + 85));
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private class Widok extends JPanel {
		private static final long serialVersionUID = 689343879959634620L;

		public void paint(Graphics g) {
			drawPlansza(g);
		}

		private void drawPlansza(Graphics g) {
			int count = 0;
			int mCount = 0;
			for (int i = 0; i < plansza.length; i++) {
				for (int j = 0; j < plansza[i].length; j++) {
					if (!plansza[i][j].isShowed())
						count++;
					if (plansza[i][j].isMark())
						mCount++;
					g.setColor(plansza[i][j].getColor());
					if (!plansza[i][j].isShowed()) {
						g.fill3DRect(i * 15 + 17, j * 15 + 10, 15, 15, false);
					} else {
						if (!started) {
							startTime = System.currentTimeMillis();
							started = true;
						}
						if (plansza[i][j].isBomba() && !plansza[i][j].isMark()) {
							g.setColor(Color.RED);
							koniec = true;
						}
						g.fill3DRect(i * 15 + 17, j * 15 + 10, 15, 15, true);
						g.setColor(Color.BLACK);
						if (!plansza[i][j].isBomba()
								&& plansza[i][j].getIleBomb() != 0) {
							g.drawString(plansza[i][j].getIleBomb() + "",
									i * 15 + 20, j * 15 + 21);
							g.drawString(plansza[i][j].getIleBomb() + "",
									i * 15 + 21, j * 15 + 21);
						}

					}
				}
			}
			if (count == ileBomb) {
				koniec = true;
				for (int i = 0; i < plansza.length; i++) {
					for (int j = 0; j < plansza[i].length; j++) {
						if (plansza[i][j].isBomba()) {
							g.setColor(Color.BLUE);
							g.fill3DRect(i * 15 + 17, j * 15 + 10, 15, 15, true);
						}
					}
				}
			}
			if (ileBomb - mCount >= 0)
				bombsLeft.setText((ileBomb - mCount) + "");
			else
				bombsLeft.setText("- " + (ileBomb - mCount) * (-1) + "");
		}
	}

	private class AListener implements ActionListener {

		public void actionPerformed(ActionEvent a) {
			if (a.getSource() == restart) {
				try {
					generuj();
				} catch (BombException e) {
					e.printStackTrace();
				}
				zliczBomby();
				time.setText("0s");
				koniec = false;
				started = false;
				repaint();
			}
			if (a.getSource() == settingsWindow) {
				new SettingsView(view);
			}
			if (a.getSource() == scoresWindow) {
				new ScoreView();
			}
		}

	}

	private class MListener implements MouseListener {
		public void mouseClicked(MouseEvent e) {
			if (!koniec) {
				int x = e.getX() / 15 - 1;
				int y = e.getY() / 15 - 1;
				if (x >= 0 && x < plansza.length && y >= 0
						&& y < plansza[x].length) {
					Pole t = plansza[x][y];
					if (e.getButton() == 1) {
						if (!t.isMark()) {
							t.setShowed(true);
							if (t.getIleBomb() == 0) {
								sprawdzPuste(x, y);
							}
						}
					}
					if (e.getButton() == 3) {
						if (!t.isShowed()) {
							t.setMark(!t.isMark());
							if (t.isMark())
								t.setColor(Color.BLUE);
							else
								t.setColor(Color.LIGHT_GRAY);
						}
					}
				}
				repaint();
			}
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

	}

	protected void generuj() throws BombException {
		plansza = new Pole[xPos][yPos];
		for (int i = 0; i < plansza.length; i++) {
			for (int j = 0; j < plansza[i].length; j++) {
				plansza[i][j] = new Pole();
				plansza[i][j].setIleBomb(0);
			}
		}
		if (ileBomb > xPos * yPos || ileBomb < 0)
			throw new BombException();
		for (int i = 0; i < ileBomb; i++) {
			Random random = new Random();
			int tx = random.nextInt(plansza.length);
			int ty = random.nextInt(plansza[0].length);
			Pole tpole = plansza[tx][ty];
			if (tpole.isBomba()) {
				i--;
			} else {
				tpole.setBomba(true);
			}
		}
		zliczBomby();
	}

	private void sprawdzPuste(int x, int y) {
		if (!plansza[x][y].isBomba()) {
			if (x > 0 && y > 0 && !plansza[x - 1][y - 1].isMark()) {
				Pole t = plansza[x - 1][y - 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x - 1, y - 1);
				}
			}
			if (x > 0 && y + 1 < plansza[x].length
					&& !plansza[x - 1][y + 1].isMark()) {
				Pole t = plansza[x - 1][y + 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x - 1, y + 1);
				}
			}
			if (y > 0 && x + 1 < plansza.length
					&& !plansza[x + 1][y - 1].isMark()) {
				Pole t = plansza[x + 1][y - 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x + 1, y - 1);
				}
			}
			if (x + 1 < plansza.length && y + 1 < plansza[x].length
					&& !plansza[x + 1][y + 1].isMark()) {
				Pole t = plansza[x + 1][y + 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x + 1, y + 1);
				}
			}
			if (x > 0 && !plansza[x - 1][y].isMark()) {
				Pole t = plansza[x - 1][y];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x - 1, y);
				}
			}
			if (x + 1 < plansza.length && !plansza[x + 1][y].isMark()) {
				Pole t = plansza[x + 1][y];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x + 1, y);
				}
			}
			if (y > 0 && !plansza[x][y - 1].isMark()) {
				Pole t = plansza[x][y - 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x, y - 1);
				}
			}
			if (y + 1 < plansza[x].length && !plansza[x][y + 1].isMark()) {
				Pole t = plansza[x][y + 1];
				if (!t.isShowed()) {
					t.setShowed(true);
					if (t.getIleBomb() == 0)
						sprawdzPuste(x, y + 1);
				}
			}
		}
	}

	private void zliczBomby() {
		for (int i = 0; i < plansza.length; i++) {
			for (int j = 0; j < plansza[i].length; j++) {
				plansza[i][j].setIleBomb(0);
			}
		}
		for (int i = 0; i < plansza.length; i++) {
			for (int j = 0; j < plansza[i].length; j++) {
				if (plansza[i][j].isBomba()) {
					if (i > 0 && j > 0 && !plansza[i - 1][j - 1].isBomba())
						plansza[i - 1][j - 1].setIleBomb(plansza[i - 1][j - 1]
								.getIleBomb() + 1);
					if (i > 0 && !plansza[i - 1][j].isBomba())
						plansza[i - 1][j].setIleBomb(plansza[i - 1][j]
								.getIleBomb() + 1);
					if (i > 0 && j + 1 < plansza[i].length
							&& !plansza[i - 1][j + 1].isBomba())
						plansza[i - 1][j + 1].setIleBomb(plansza[i - 1][j + 1]
								.getIleBomb() + 1);
					if (j > 0 && !plansza[i][j - 1].isBomba())
						plansza[i][j - 1].setIleBomb(plansza[i][j - 1]
								.getIleBomb() + 1);
					if (j + 1 < plansza[i].length
							&& !plansza[i][j + 1].isBomba())
						plansza[i][j + 1].setIleBomb(plansza[i][j + 1]
								.getIleBomb() + 1);
					if (i + 1 < plansza.length && !plansza[i + 1][j].isBomba())
						plansza[i + 1][j].setIleBomb(plansza[i + 1][j]
								.getIleBomb() + 1);
					if (i + 1 < plansza.length && j + 1 < plansza[i].length
							&& !plansza[i + 1][j + 1].isBomba())
						plansza[i + 1][j + 1].setIleBomb(plansza[i + 1][j + 1]
								.getIleBomb() + 1);
					if (i + 1 < plansza.length && j > 0
							&& !plansza[i + 1][j - 1].isBomba())
						plansza[i + 1][j - 1].setIleBomb(plansza[i + 1][j - 1]
								.getIleBomb() + 1);
				}
			}
		}
	}

}
