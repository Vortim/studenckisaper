package saper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;

public class SettingsView extends JFrame implements Serializable {
	private static final long serialVersionUID = 4639884409924987861L;

	private JPanel panel = new JPanel();
	private JTextField bombs = new JTextField();
	private JTextField x = new JTextField();
	private JTextField y = new JTextField();
	private JRadioButton custom = new JRadioButton("Custom");
	private JRadioButton easy = new JRadioButton("Easy");
	private JRadioButton medium = new JRadioButton("Medium");
	private JRadioButton hard = new JRadioButton("Hard");
	private ButtonGroup bGroup = new ButtonGroup();
	private JPanel feedLine1 = new JPanel();
	private JPanel feedLine2 = new JPanel();
	private JPanel feedLine3 = new JPanel();
	private JPanel feedLine4 = new JPanel();
	private JButton okButton = new JButton("OK");
	private final View view;

	public SettingsView(View view) {
		this.view = view;
		setContentPane(panel);
		panel.setLayout(new FlowLayout());
		panel.setPreferredSize(new Dimension(380, 200));
		panel.add(easy);
		easy.addActionListener(new Listener());
		easy.setBackground(Color.LIGHT_GRAY);
		panel.add(medium);
		medium.addActionListener(new Listener());
		medium.setBackground(Color.LIGHT_GRAY);
		panel.add(hard);
		hard.addActionListener(new Listener());
		hard.setBackground(Color.LIGHT_GRAY);
		panel.add(custom);
		custom.addActionListener(new Listener());
		custom.setBackground(Color.LIGHT_GRAY);
		feedLine1.setPreferredSize(new Dimension(350, 0));
		feedLine2.setPreferredSize(new Dimension(350, 0));
		feedLine3.setPreferredSize(new Dimension(350, 0));
		feedLine4.setPreferredSize(new Dimension(350, 0));
		panel.add(feedLine1);
		panel.add(new JLabel("        Bomby: "));
		panel.add(bombs);
		bombs.addKeyListener(new KListener());
		panel.add(feedLine2);
		panel.add(new JLabel(" Szeroko��: "));
		panel.add(x);
		x.addKeyListener(new KListener());
		panel.add(feedLine3);
		panel.add(new JLabel(" Wysoko��: "));
		panel.add(y);
		y.addKeyListener(new KListener());
		panel.add(feedLine4);
		panel.add(okButton);
		custom.setSelected(true);
		okButton.addActionListener(new Listener());
		bGroup.add(custom);
		bGroup.add(easy);
		bGroup.add(medium);
		bGroup.add(hard);
		bombs.setText("");
		bombs.setPreferredSize(new Dimension(50, 25));
		x.setText("");
		x.setPreferredSize(new Dimension(50, 25));
		y.setText("");
		y.setPreferredSize(new Dimension(50, 25));
		panel.setBackground(Color.LIGHT_GRAY);
		setSize(350, 205);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void setEnabledTextFields(boolean enabled) {
		bombs.setEditable(enabled);
		x.setEditable(enabled);
		y.setEditable(enabled);
	}

	public class KListener implements KeyListener {

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
		}

		public void keyTyped(KeyEvent e) {
			try {
				if (e.getSource() == bombs) {
					Integer.parseInt(bombs.getText() + e.getKeyChar() + "");
				}
			} catch (NumberFormatException z) {
				bombs.setText("");
			}
			try {
				if (e.getSource() == x) {
					Integer.parseInt(x.getText() + e.getKeyChar() + "");
				}
			} catch (NumberFormatException z) {
				x.setText("");
			}
			try {
				if (e.getSource() == y) {
					Integer.parseInt(y.getText() + e.getKeyChar() + "");
				}
			} catch (NumberFormatException z) {
				y.setText("");
			}
		}
	}

	public class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				int ileBomb = Integer.parseInt(bombs.getText());
				int xPos = Integer.parseInt(x.getText());
				int yPos = Integer.parseInt(y.getText());
				if (e.getSource() == okButton) {
					if (ileBomb < xPos * yPos && xPos > 0 && yPos > 0 && xPos < 31 && yPos < 31) {
						view.ileBomb = ileBomb;
						view.xPos = xPos;
						view.yPos = yPos;
						view.koniec = false;
						try {
							view.generuj();
						} catch (BombException e1) {
							// TODO Auto-generated catch block
						}
						view.repaint();
						setVisible(false);
					}
				}
			} catch (NumberFormatException z) {
				// TODO z.printStackTrace();
			}
			if (e.getSource() == custom) {
				setEnabledTextFields(true);
			}
			if (e.getSource() == easy || e.getSource() == medium
					|| e.getSource() == hard) {
				setEnabledTextFields(false);
				if (e.getSource() == easy) {
					bombs.setText("10");
					x.setText("9");
					y.setText("9");
				}
				if (e.getSource() == medium) {
					bombs.setText("40");
					x.setText("16");
					y.setText("16");
				}
				if (e.getSource() == hard) {
					bombs.setText("99");
					x.setText("30");
					y.setText("16");
				}
			}
		}
	}
}
